const fs = require("fs");
const PNG = require("pngjs").PNG;

const imagePath = "images/";
const outDir = "out_images/";

if (!fs.existsSync(imagePath)) {
    console.log("There is no images directory! It was created for you.");
    fs.mkdirSync(imagePath);
    process.exit(1);
}
if (!fs.existsSync(outDir)) {
    fs.mkdirSync(outDir);
}

if (!fs.existsSync("dead-pixels.json")) {
    console.log("There is no 'dead-pixels.json' file!");
    process.exit(1);
}

const pixelData = JSON.parse(fs.readFileSync("dead-pixels.json"));
if (!pixelData.images) {
    console.log("'dead-pixels.json' is not valid!");
    process.exit(1);
}

const imageFileNames = fs.readdirSync(imagePath).filter(file => file !== ".DS_Store");

console.log(imageFileNames);

function setMarkPixel(x, y, intensity, img) {
    if (x >= 0 && y >= 0 && x < img.width && y < img.height) {
        const id = (img.width * y + x) << 2;
        const value = map(intensity, 0, 255, 10, 255);
        img.data[id] = value; img.data[id + 1] = value; img.data[id + 2] = value;
    }
}

function setWhite(x, y, img, i) {
    if (x >= 0 && y >= 0 && x < img.width && y < img.height) {
        const id = (img.width * y + x) << 2;
        const value = i % 5 * 50 + 50;
        img.data[id] = value; img.data[id + 1] = value; img.data[id + 2] = value;
    }
}

function map(val, v_min, v_max, min, max) {
    return ((val - v_min) / (v_max - v_min)) * (max - min) + min;
}

function mark(x, y, intensity, r, img) {
    const id = (x, y) => (img.width * y + x) << 2;

    let b1 = y + r, b2 = y - r; // y
    for (let i = 0; i < 2 * r; i++) { // horizontal top
        setMarkPixel(x - r + i, b1, intensity, img);
    }
    for (let i = 0; i < 2 * r; i++) { // horizontal bot
        setMarkPixel(x - r + i, b2, intensity, img);
    }
    let a1 = x + r, a2 = x - r; // y
    for (let i = 0; i <= 2 * r; i++) { // vertical lft
        setMarkPixel(a1, y - r + i, intensity, img);
    }
    for (let i = 0; i <= 2 * r; i++) { // vertical rgt
        setMarkPixel(a2, y - r + i, intensity, img);
    }
    //mark white
    b1 = y + 2 * r, b2 = y - 2 * r; // y
    for (let i = 0; i < 4 * r; i++) { // horizontal top
        setWhite(x - 2 * r + i, b1, img, i);
    }
    for (let i = 0; i < 4 * r; i++) { // horizontal bot
        setWhite(x - 2 * r + i, b2, img, i);
    }
    a1 = x + 2 * r, a2 = x - 2 * r; // y
    for (let i = 0; i <= 4 * r; i++) { // vertical lft
        setWhite(a1, y - 2 * r + i, img, i);
    }
    for (let i = 0; i <= 4 * r; i++) { // vertical rgt
        setWhite(a2, y - 2 * r + i, img, i);
    }
}

function drawRect(img, value, x, y, r) {
    for (let a = 0; a < r; a++) {
        for (let b = 0; b < r; b++) {
            const id = (img.width * (b + y) + a + x) << 2;
            img.data[id] = value; img.data[id + 1] = value; img.data[id + 2] = value;
        }
    }
}

function addLegend(img) {
    for (let i = 1; i <= 5; i++) {
        const val = 50 * i;
        const x = 20, y = img.height - i * 60;
        drawRect(img, val, x, y, 40);
    }
}

function flatten(arr) {
    const newArr = [];
    for (const a of arr) {
        newArr.push(...a);
    }
    return newArr;
}

for (const imageFileName of imageFileNames) {
    const camId = "C" + imageFileName.split("_")[1].substring(2, 5);
    const dataImages = pixelData.images.map(img => img.id).filter(img => img.startsWith(camId));
    const imageDatas = pixelData.images.filter(image => image.id === dataImages[dataImages.length - 1]);
    fs.createReadStream(imagePath + imageFileName)
        .pipe(new PNG({
            filterType: 4
        }))
        .on('parsed', function () {
            // console.log(imageDatas)
            if (imageDatas.length) {
                const dataset = imageDatas[0];
                const deadPixels = dataset.deadPixelGroups
                    ? flatten(dataset.deadPixelGroups)
                    : dataset.deadPixels;
                console.log(imageFileName, deadPixels.length)
                // console.log(this)
                for (const pxl of deadPixels) {
                    mark(pxl.x, pxl.y, pxl.value, 8, this);
                }
            } else return;
            addLegend(this);
            this.pack().pipe(fs.createWriteStream(outDir + imageFileName));
        });
}
